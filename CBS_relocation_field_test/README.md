# Videos

These are the videos used for user instructions. The compressed versions were shipped in the app itself and are shown here for preview.

The uncompressed versions are too large to be show as a preview here, but can be found in the [videos/uncompressed](videos/uncompressed) folder instead.

<details>
  <summary><h2>Android</h2></summary>

### Daily Overview
![Download Daily Overview](videos/compressed/android/Dagoverzicht.mp4)

### Locations
![Download Locations](videos/compressed/android/Location.mp4)

### Motives
![Download Motives](videos/compressed/android/Motive.m4v)

### Transportation Modes
![Download Transportation Modes](videos/compressed/android/Transportation_modes.mp4)

### Settings
![Download Settings](videos/compressed/android/Settings_2.mp4)
</details>

<details>
  <summary><h2>iOS</h2></summary>

### Daily Overview
![Download Daily Overview](videos/compressed/iOS/Dagoverzicht_ios.m4v)

### Locations
![Download Locations](videos/compressed/iOS/Location_ios.m4v)

### Motives
![Download Motives](videos/compressed/iOS/Motive_ios.m4v)

### Transportation Modes
![Download Transportation Modes](videos/compressed/iOS/Transportation_modes_ios.m4v)

### Settings
![Download Settings](videos/compressed/iOS/Settings_ios.m4v)
</details>

Ole's Salad
===========


Ingredients
-----------

* salad
	- rocket salad leaves
	- field salad leaves
	- cherry tomatoes
	- avocado
	- buffalo mozzarella (mini, if available)
	- oyster mushrooms
	- shiitake mushrooms (optional)
	- garlic
	- pepper (freshly ground)

* sauce
	- olive oil
	- mild vinegar (Japanese rice vinegar, if available)
	- garlic
	- sesame oil
	- soy sauce
	- pepper (freshly ground)
	- herbs (freshly chopped or dried)


Assembly
--------

1. Clean mushrooms and cut into bite-sized pieces. Toss into a pan, add a pressed clove of garlic and salt slightly. Fry with a dash of cooking oil on high heat until golden-brown. Take off the fire and season with freshly ground pepper.

2. While the mushrooms are sizzling in the pan, clean and chop the cherry tomatoes, avocado and buffalo mozzarella into bite-sized pieces.

3. Wash the salad leaves. Combine rocket and field salad 50/50 in a salad bowl. Add chopped ingredients. Let the mushrooms cool for a bit (otherwise they will melt the cheese) and combine all in the bowl.

4. Press one garlic clove into a lockable glass jar. Add olive oil and dashes of mild vinegar, and sesame oil. Season to taste with pepper, herbs, and soy sauce instead of salt. Close jar and shake it into a vinaigrette.

5. Add the vinaigrette right before serving.

6. Enjoy!

Tabi Documentation
==================

Here you can find documentation about the Tabi Project, the app or the backend, or data collection with apps in general. 


## contribute

Did your institute conduct proofs-of-concept with apps? Please let us know! Others can benefit from your experience. To get your documents in this repository, use email or a merge request. If you never worked with GitLab or GitHub before, just email the documents to <WINHelpdesk@cbs.nl>. More elegant than email is a merge request. Fork this repository and put your documents and licenses in the right place. Then issue a merge request. Details can be found in the [GitLab documentation](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).


## license

Make sure to include a license. Otherwise users don't know what they are allowed to do with your documents. A safe pick is one of the [Creative Commons](https://creativecommons.org/licenses/), for example the [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Here is an [overview](https://en.wikipedia.org/wiki/Creative_Commons_license#Seven_regularly_used_licenses) to help you choose.


## folder structure

Please pool documents about the same topic in one folder. To help others to put your documentation in context, we ask you to also include a `README.md` document about that topic. The `README.md` is written in the Markdown syntax. Here's a [quick overview](https://en.wikipedia.org/wiki/Markdown#Example) or an [in-depth guide](https://daringfireball.net/projects/markdown/syntax).

```
CBS_Proof_of_Concept_Budget_Spending_App
╠═ CBS_Proof_of_Concept_Budget_Spending_App_Project_Plan.txt
╠═ CBS_Proof_of_Concept_Budget_Spending_App_Project_Plan.LICENSE
╠═ CBS_Proof_of_Concept_Budget_Spending_App_Research_Outcome.pdf
╠═ CBS_Proof_of_Concept_Budget_Spending_App_Research_Outcome.LICENSE
╚═ README.md
```


## file formats

We recommend to adhere to [Standford's Libraries best practices for file formats](https://library.stanford.edu/research/data-management-services/data-best-practices/best-practices-file-formats) and use non-proprietary (open) file formats when possible. This ensures that everybody can open and read your documents, now and in the future. You can find a non-exhaustive list for text files on [Wikipedia: List of open formats](https://en.wikipedia.org/wiki/List_of_open_formats#Text).

This is a recommendation, not a rule. If you can not use open formats, don't let that keep you from sharing your documents! Every bit helps spreading the knowledge.


## example

You can find an example contribution in the [examples](examples) folder.
